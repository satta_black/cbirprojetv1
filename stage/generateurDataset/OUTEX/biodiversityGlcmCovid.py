import cv2
from mahotas import features
from os import listdir
from typing import List
from scipy.spatial import distance
import pandas as pd
from skimage.feature import graycomatrix, graycoprops
import numpy as np
from BiT import bio_taxo, biodiversity, taxonomy



#Image Dataset
path = 'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/image/OUTEX/'
image_dir: List[str] = listdir(path)
List_of_data1 = list()
image_dir: List[str] = listdir(path)


def bitGlcm(file, dossier, fichier):
    glcm_feat = glcm(file) # list
    bit_feat = BiT(file) # list
    f = glcm_feat + bit_feat
    final_list = np.append(dossier, fichier)
    final_list = np.append(final_list, f)
    return final_list

def BiT(file):
    bit_features = biodiversity(file)
    return bit_features # 14 features

def glcm(file) :
    # skimage.feature.texture.greycomatrix(image, distances, angles, levels=256, symmetric=False, normed=False)
    features = []
    angles = [0, np.pi / 2, np.pi / 4]
    for angle in angles :
        print('Angle: ', angle)
        co_matrix = graycomatrix(file, [5], [angle], levels=256, symmetric=True, normed=True)
        diss = graycoprops(co_matrix, 'dissimilarity')[0, 0]
        features.append(diss)
        cont = graycoprops(co_matrix, 'contrast')[0, 0]
        features.append(cont)
        ener = graycoprops(co_matrix, 'energy')[0, 0]
        features.append(ener)
        homo = graycoprops(co_matrix, 'homogeneity')[0, 0]
        features.append(homo)
        corr = graycoprops(co_matrix, 'correlation')[0, 0]
        features.append(corr)
        asm = graycoprops(co_matrix, 'ASM')[0, 0]
        features.append(asm)
    return features # 18 features

iteration = 1
ListOfList_features = list()
for dossier in image_dir:
    for fichier in listdir(path + dossier):
        path3 = path + dossier + '/' + fichier
        img = cv2.imread(path3, 0)
        img = cv2.resize(img, (202, 144))
        # Feature extraction
        haralick_vector = bitGlcm(img, dossier, fichier)
        ListOfList_features.append(haralick_vector)
    iteration += 1

print('Start generating a dataframe ->')
df_final = pd.DataFrame.from_records(data=ListOfList_features)
print('Done!')
df_final.to_csv('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityGlcmOUTEX.csv', sep=',', index=False)



