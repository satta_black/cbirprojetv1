import cv2
from mahotas import features
from os import listdir
from typing import List
from scipy.spatial import distance
import pandas as pd
from skimage.feature import graycomatrix, graycoprops
import numpy as np
from BiT import bio_taxo, biodiversity, taxonomy



#Image Dataset
path = 'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/image/GLAUCOMA/'
image_dir: List[str] = listdir(path)
List_of_data1 = list()
image_dir: List[str] = listdir(path)


def bitHaralick(file, dossier, fichier):
    haralick_feat = haralick(file) # list
    bit_feat = BiT(file) # list
    f = haralick_feat + bit_feat
    final_list = np.append(dossier, fichier)
    final_list = np.append(final_list, f)
    return final_list

def BiT(file):
    bit_features = biodiversity(file)
    return bit_features # 14 features

def haralick(file):
    # return features.haralick(file)
    haralick_features = features.haralick(file)
    haralick_features = [feat for sublist in haralick_features for feat in sublist]
    return haralick_features # 52 features

iteration = 1
ListOfList_features = list()
for dossier in image_dir:
    for fichier in listdir(path + dossier):
        path3 = path + dossier + '/' + fichier
        img = cv2.imread(path3, 0)
        img = cv2.resize(img, (202, 144))
        # Feature extraction
        haralick_vector = bitHaralick(img, dossier, fichier)
        ListOfList_features.append(haralick_vector)
    iteration += 1

print('Start generating a dataframe ->')
df_final = pd.DataFrame.from_records(data=ListOfList_features)
print('Done!')
df_final.to_csv('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityHaralickGLAUCOMA.csv', sep=',', index=False)



