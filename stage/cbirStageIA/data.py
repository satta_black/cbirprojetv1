import cv2,csv
from mahotas import features
from os import listdir
from typing import List
from scipy.spatial import distance
import pandas as pd
import numpy as np
from scipy.spatial import distance
from skimage.feature import graycomatrix, graycoprops
from BiT import bio_taxo, biodiversity, taxonomy


def haralickEuc(img, nombre, classe):
    haralick_features = features.haralick(img)
    haralick_features = [feat for sublist in haralick_features for feat in sublist]

    # lecture des fichier csv
    haraCovid = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/haralickCovid.csv', 'r')
    haraCovidOpen = csv.reader(haraCovid)

    haraFMD = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/haralickFMD.csv', 'r')
    haraFMDOpen = csv.reader(haraFMD)

    haraGLAUCOMA = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/haralickGLAUCOMA.csv', 'r')
    haraGLAUCOMAOpen = csv.reader(haraGLAUCOMA)

    haraKTH = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/haralickKTH.csv', 'r')
    haraKTHOpen = csv.reader(haraKTH)

    haraOUTEX = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/haralickOUTEX.csv', 'r')
    haraOUTEXOpen = csv.reader(haraOUTEX)

    n = ""
    if (classe == "COVID"):
        n = 'images'
        o = haraCovidOpen
    elif (classe == "GLAUCOMA"):
        n = 'GLAUCOMA'
        o = haraGLAUCOMAOpen
    elif (classe == "KTH"):
        n = 'KTH'
        o = haraKTHOpen
    elif (classe == "OUTEX"):
        n = "OUTEX"
        o = haraOUTEXOpen



    listTotal = []
    listTotal2 = []
    if(n!=""):
        for r in o:
            listD = r[0]
            listN = r[1]
            list = r[2:]
            float_list = []

            for item in list:
                float_list.append(float(item))

            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.euclidean(float_list, haralick_features)
            valeur_list = [listD, listN, dist, 'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/image/' + n + '/' + listD + '/' + listN]
            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)

        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0

        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        print(nb)
        print(nombre)
        pourc = float(nb) * 100 / float(nombre)

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'], listTotal[i]['url'],
                            nb, pourc]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)


    if (classe == "TOUT"):
        listTotal = haralickEuc(img, "1400", "COVID")
        listTotal += haralickEuc(img, "900", "OUTEX")
        listTotal += haralickEuc(img, "500", "GLAUCOMA")
        listTotal += haralickEuc(img, "810", "KTH")


        def get_dist(listTotal):
            return listTotal.get('distance')
        listTotal.sort(key=get_dist)
        nb = 0
        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'], listTotal[i]['url'],
                            nb, listTotal[i]['pourcentage']]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

        listTotal2[0]['nbCovid'] = nb

    return listTotal2

#



def haralickCan(img, nombre, classe):
    haralick_features = features.haralick(img)
    haralick_features = [feat for sublist in haralick_features for feat in sublist]

    # lecture des fichier csv
    haraCovid = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/haralickCovid.csv', 'r')
    haraCovidOpen = csv.reader(haraCovid)

    haraFMD = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/haralickFMD.csv', 'r')
    haraFMDOpen = csv.reader(haraFMD)

    haraGLAUCOMA = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/haralickGLAUCOMA.csv', 'r')
    haraGLAUCOMAOpen = csv.reader(haraGLAUCOMA)

    haraKTH = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/haralickKTH.csv', 'r')
    haraKTHOpen = csv.reader(haraKTH)

    haraOUTEX = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/haralickOUTEX.csv', 'r')
    haraOUTEXOpen = csv.reader(haraOUTEX)

    n = ""
    if (classe == "COVID"):
        n = 'images'
        o = haraCovidOpen
    elif (classe == "GLAUCOMA"):
        n = 'GLAUCOMA'
        o = haraGLAUCOMAOpen
    elif (classe == "KTH"):
        n = 'KTH'
        o = haraKTHOpen
    elif (classe == "OUTEX"):
        n = "OUTEX"
        o = haraOUTEXOpen



    listTotal = []
    listTotal2 = []
    if(n!=""):
        for r in o:
            listD = r[0]
            listN = r[1]
            list = r[2:]
            float_list = []

            for item in list:
                float_list.append(float(item))

            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.canberra(float_list, haralick_features)
            valeur_list = [listD, listN, dist, 'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/image/' + n + '/' + listD + '/' + listN]
            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)

        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0

        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        print(nb)
        print(nombre)
        pourc = float(nb) * 100 / float(nombre)

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'], listTotal[i]['url'],
                            nb, pourc]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)


    if (classe == "TOUT"):
        listTotal = haralickCan(img, "1400", "COVID")
        listTotal += haralickCan(img, "900", "OUTEX")
        listTotal += haralickCan(img, "500", "GLAUCOMA")
        listTotal += haralickCan(img, "810", "KTH")


        def get_dist(listTotal):
            return listTotal.get('distance')
        listTotal.sort(key=get_dist)
        nb = 0
        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'], listTotal[i]['url'],
                            nb, listTotal[i]['pourcentage']]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

        listTotal2[0]['nbCovid'] = nb

    return listTotal2

#




def haralickChe(img, nombre, classe):
    haralick_features = features.haralick(img)
    haralick_features = [feat for sublist in haralick_features for feat in sublist]

    # lecture des fichier csv
    haraCovid = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/haralickCovid.csv', 'r')
    haraCovidOpen = csv.reader(haraCovid)

    haraFMD = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/haralickFMD.csv', 'r')
    haraFMDOpen = csv.reader(haraFMD)

    haraGLAUCOMA = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/haralickGLAUCOMA.csv', 'r')
    haraGLAUCOMAOpen = csv.reader(haraGLAUCOMA)

    haraKTH = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/haralickKTH.csv', 'r')
    haraKTHOpen = csv.reader(haraKTH)

    haraOUTEX = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/haralickOUTEX.csv', 'r')
    haraOUTEXOpen = csv.reader(haraOUTEX)

    n = ""
    if (classe == "COVID"):
        n = 'images'
        o = haraCovidOpen
    elif (classe == "GLAUCOMA"):
        n = 'GLAUCOMA'
        o = haraGLAUCOMAOpen
    elif (classe == "KTH"):
        n = 'KTH'
        o = haraKTHOpen
    elif (classe == "OUTEX"):
        n = "OUTEX"
        o = haraOUTEXOpen



    listTotal = []
    listTotal2 = []
    if(n!=""):
        for r in o:
            listD = r[0]
            listN = r[1]
            list = r[2:]
            float_list = []

            for item in list:
                float_list.append(float(item))

            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.chebyshev(float_list, haralick_features)
            valeur_list = [listD, listN, dist, 'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/image/' + n + '/' + listD + '/' + listN]
            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)

        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0

        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        print(nb)
        print(nombre)
        pourc = float(nb) * 100 / float(nombre)

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'], listTotal[i]['url'],
                            nb, pourc]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)


    if (classe == "TOUT"):
        listTotal = haralickChe(img, "1400", "COVID")
        listTotal += haralickChe(img, "900", "OUTEX")
        listTotal += haralickChe(img, "500", "GLAUCOMA")
        listTotal += haralickChe(img, "810", "KTH")


        def get_dist(listTotal):
            return listTotal.get('distance')
        listTotal.sort(key=get_dist)
        nb = 0
        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'], listTotal[i]['url'],
                            nb, listTotal[i]['pourcentage']]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

        listTotal2[0]['nbCovid'] = nb

    return listTotal2

#




def haralickMan(img, nombre, classe):
    haralick_features = features.haralick(img)
    haralick_features = [feat for sublist in haralick_features for feat in sublist]

    # lecture des fichier csv
    haraCovid = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/haralickCovid.csv', 'r')
    haraCovidOpen = csv.reader(haraCovid)

    haraFMD = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/haralickFMD.csv', 'r')
    haraFMDOpen = csv.reader(haraFMD)

    haraGLAUCOMA = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/haralickGLAUCOMA.csv', 'r')
    haraGLAUCOMAOpen = csv.reader(haraGLAUCOMA)

    haraKTH = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/haralickKTH.csv', 'r')
    haraKTHOpen = csv.reader(haraKTH)

    haraOUTEX = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/haralickOUTEX.csv', 'r')
    haraOUTEXOpen = csv.reader(haraOUTEX)

    n = ""
    if (classe == "COVID"):
        n = 'images'
        o = haraCovidOpen
    elif (classe == "GLAUCOMA"):
        n = 'GLAUCOMA'
        o = haraGLAUCOMAOpen
    elif (classe == "KTH"):
        n = 'KTH'
        o = haraKTHOpen
    elif (classe == "OUTEX"):
        n = "OUTEX"
        o = haraOUTEXOpen



    listTotal = []
    listTotal2 = []
    if(n!=""):
        for r in o:
            listD = r[0]
            listN = r[1]
            list = r[2:]
            float_list = []

            for item in list:
                float_list.append(float(item))

            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.cityblock(float_list, haralick_features)
            valeur_list = [listD, listN, dist, 'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/image/' + n + '/' + listD + '/' + listN]
            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)

        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0

        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        print(nb)
        print(nombre)
        pourc = float(nb) * 100 / float(nombre)

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'], listTotal[i]['url'],
                            nb, pourc]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)


    if (classe == "TOUT"):
        listTotal = haralickMan(img, "1400", "COVID")
        listTotal += haralickMan(img, "900", "OUTEX")
        listTotal += haralickMan(img, "500", "GLAUCOMA")
        listTotal += haralickMan(img, "810", "KTH")


        def get_dist(listTotal):
            return listTotal.get('distance')
        listTotal.sort(key=get_dist)
        nb = 0
        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'], listTotal[i]['url'],
                            nb, listTotal[i]['pourcentage']]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

        listTotal2[0]['nbCovid'] = nb

    return listTotal2
#



def GLCMEuc(img, nombre, classe):
    haralick_features = []
    angles = [0, np.pi / 2, np.pi / 4]
    for angle in angles:
        print('Angle: ', angle)
        co_matrix = graycomatrix(img, [5], [angle], levels=256, symmetric=True, normed=True)
        diss = graycoprops(co_matrix, 'dissimilarity')[0, 0]
        haralick_features.append(diss)
        cont = graycoprops(co_matrix, 'contrast')[0, 0]
        haralick_features.append(cont)
        ener = graycoprops(co_matrix, 'energy')[0, 0]
        haralick_features.append(ener)
        homo = graycoprops(co_matrix, 'homogeneity')[0, 0]
        haralick_features.append(homo)
        corr = graycoprops(co_matrix, 'correlation')[0, 0]
        haralick_features.append(corr)
        asm = graycoprops(co_matrix, 'ASM')[0, 0]
        haralick_features.append(asm)

    # lecture des fichier csv
    glcmCovid = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/glcmCovid.csv', 'r')
    glcmCovidOpen = csv.reader(glcmCovid)

    glcmFMD = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/glcmFMD.csv', 'r')
    glcmFMDOpen = csv.reader(glcmFMD)

    glcmGLAUCOMA = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/glcmGLAUCOMA.csv', 'r')
    glcmGLAUCOMAOpen = csv.reader(glcmGLAUCOMA)

    glcmKTH = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/glcmKTH.csv', 'r')
    glcmKTHOpen = csv.reader(glcmKTH)

    glcmOUTEX = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/glcmOUTEX.csv', 'r')
    glcmOUTEXOpen = csv.reader(glcmOUTEX)

    n = ""
    if (classe == "COVID"):
        n = 'images'
        o = glcmCovidOpen
    elif (classe == "GLAUCOMA"):
        n = 'GLAUCOMA'
        o = glcmGLAUCOMAOpen
    elif (classe == "KTH"):
        n = 'KTH'
        o = glcmKTHOpen
    elif (classe == "OUTEX"):
        n = "OUTEX"
        o = glcmOUTEXOpen


    listTotal = []
    listTotal2 = []
    if(n!=""):
        for r in o:
            listD = r[0]
            listN = r[1]
            list = r[2:]
            float_list = []

            for item in list:
                float_list.append(float(item))

            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.euclidean(float_list, haralick_features)
            valeur_list = [listD, listN, dist, 'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/image/' + n + '/' + listD + '/' + listN]
            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)

        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0

        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        print(nb)
        print(nombre)
        pourc = float(nb) * 100 / float(nombre)

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'], listTotal[i]['url'],
                            nb, pourc]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)


    if (classe == "TOUT"):
        listTotal = GLCMEuc(img, "1400", "COVID")
        listTotal += GLCMEuc(img, "900", "OUTEX")
        listTotal += GLCMEuc(img, "500", "GLAUCOMA")
        listTotal += GLCMEuc(img, "810", "KTH")


        def get_dist(listTotal):
            return listTotal.get('distance')
        listTotal.sort(key=get_dist)
        nb = 0
        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'], listTotal[i]['url'],
                            nb, listTotal[i]['pourcentage']]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

        listTotal2[0]['nbCovid'] = nb

    return listTotal2
#






def GLCMCan(img, nombre, classe):
    haralick_features = []
    angles = [0, np.pi / 2, np.pi / 4]
    for angle in angles:
        print('Angle: ', angle)
        co_matrix = graycomatrix(img, [5], [angle], levels=256, symmetric=True, normed=True)
        diss = graycoprops(co_matrix, 'dissimilarity')[0, 0]
        haralick_features.append(diss)
        cont = graycoprops(co_matrix, 'contrast')[0, 0]
        haralick_features.append(cont)
        ener = graycoprops(co_matrix, 'energy')[0, 0]
        haralick_features.append(ener)
        homo = graycoprops(co_matrix, 'homogeneity')[0, 0]
        haralick_features.append(homo)
        corr = graycoprops(co_matrix, 'correlation')[0, 0]
        haralick_features.append(corr)
        asm = graycoprops(co_matrix, 'ASM')[0, 0]
        haralick_features.append(asm)

    # lecture des fichier csv
    glcmCovid = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/glcmCovid.csv', 'r')
    glcmCovidOpen = csv.reader(glcmCovid)

    glcmFMD = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/glcmFMD.csv', 'r')
    glcmFMDOpen = csv.reader(glcmFMD)

    glcmGLAUCOMA = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/glcmGLAUCOMA.csv', 'r')
    glcmGLAUCOMAOpen = csv.reader(glcmGLAUCOMA)

    glcmKTH = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/glcmKTH.csv', 'r')
    glcmKTHOpen = csv.reader(glcmKTH)

    glcmOUTEX = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/glcmOUTEX.csv', 'r')
    glcmOUTEXOpen = csv.reader(glcmOUTEX)

    n = ""
    if (classe == "COVID"):
        n = 'images'
        o = glcmCovidOpen
    elif (classe == "GLAUCOMA"):
        n = 'GLAUCOMA'
        o = glcmGLAUCOMAOpen
    elif (classe == "KTH"):
        n = 'KTH'
        o = glcmKTHOpen
    elif (classe == "OUTEX"):
        n = "OUTEX"
        o = glcmOUTEXOpen


    listTotal = []
    listTotal2 = []
    if (n != ""):
        for r in o:
            listD = r[0]
            listN = r[1]
            list = r[2:]
            float_list = []

            for item in list:
                float_list.append(float(item))

            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.canberra(float_list, haralick_features)
            valeur_list = [listD, listN, dist,
                           'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/image/' + n + '/' + listD + '/' + listN]
            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)

        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0

        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        print(nb)
        print(nombre)
        pourc = float(nb) * 100 / float(nombre)

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, pourc]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

    if (classe == "TOUT"):
        listTotal = GLCMCan(img, "1400", "COVID")
        listTotal += GLCMCan(img, "900", "OUTEX")
        listTotal += GLCMCan(img, "500", "GLAUCOMA")
        listTotal += GLCMCan(img, "810", "KTH")


        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0
        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, listTotal[i]['pourcentage']]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

        listTotal2[0]['nbCovid'] = nb

    return listTotal2
#




def GLCMChe(img, nombre, classe):
    haralick_features = []
    angles = [0, np.pi / 2, np.pi / 4]
    for angle in angles:
        print('Angle: ', angle)
        co_matrix = graycomatrix(img, [5], [angle], levels=256, symmetric=True, normed=True)
        diss = graycoprops(co_matrix, 'dissimilarity')[0, 0]
        haralick_features.append(diss)
        cont = graycoprops(co_matrix, 'contrast')[0, 0]
        haralick_features.append(cont)
        ener = graycoprops(co_matrix, 'energy')[0, 0]
        haralick_features.append(ener)
        homo = graycoprops(co_matrix, 'homogeneity')[0, 0]
        haralick_features.append(homo)
        corr = graycoprops(co_matrix, 'correlation')[0, 0]
        haralick_features.append(corr)
        asm = graycoprops(co_matrix, 'ASM')[0, 0]
        haralick_features.append(asm)

    # lecture des fichier csv
    glcmCovid = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/glcmCovid.csv', 'r')
    glcmCovidOpen = csv.reader(glcmCovid)

    glcmFMD = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/glcmFMD.csv', 'r')
    glcmFMDOpen = csv.reader(glcmFMD)

    glcmGLAUCOMA = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/glcmGLAUCOMA.csv', 'r')
    glcmGLAUCOMAOpen = csv.reader(glcmGLAUCOMA)

    glcmKTH = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/glcmKTH.csv', 'r')
    glcmKTHOpen = csv.reader(glcmKTH)

    glcmOUTEX = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/glcmOUTEX.csv', 'r')
    glcmOUTEXOpen = csv.reader(glcmOUTEX)

    n = ""
    if (classe == "COVID"):
        n = 'images'
        o = glcmCovidOpen
    elif (classe == "GLAUCOMA"):
        n = 'GLAUCOMA'
        o = glcmGLAUCOMAOpen
    elif (classe == "KTH"):
        n = 'KTH'
        o = glcmKTHOpen
    elif (classe == "OUTEX"):
        n = "OUTEX"
        o = glcmOUTEXOpen


    listTotal = []
    listTotal2 = []
    if (n != ""):
        for r in o:
            listD = r[0]
            listN = r[1]
            list = r[2:]
            float_list = []

            for item in list:
                float_list.append(float(item))

            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.chebyshev(float_list, haralick_features)
            valeur_list = [listD, listN, dist,
                           'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/image/' + n + '/' + listD + '/' + listN]
            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)

        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0

        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        print(nb)
        print(nombre)
        pourc = float(nb) * 100 / float(nombre)

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, pourc]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

    if (classe == "TOUT"):
        listTotal = GLCMChe(img, "1400", "COVID")
        listTotal += GLCMChe(img, "900", "OUTEX")
        listTotal += GLCMChe(img, "500", "GLAUCOMA")
        listTotal += GLCMChe(img, "810", "KTH")


        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0
        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, listTotal[i]['pourcentage']]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

        listTotal2[0]['nbCovid'] = nb

    return listTotal2

#




def GLCMMan(img, nombre, classe):
    haralick_features = []
    angles = [0, np.pi / 2, np.pi / 4]
    for angle in angles:
        print('Angle: ', angle)
        co_matrix = graycomatrix(img, [5], [angle], levels=256, symmetric=True, normed=True)
        diss = graycoprops(co_matrix, 'dissimilarity')[0, 0]
        haralick_features.append(diss)
        cont = graycoprops(co_matrix, 'contrast')[0, 0]
        haralick_features.append(cont)
        ener = graycoprops(co_matrix, 'energy')[0, 0]
        haralick_features.append(ener)
        homo = graycoprops(co_matrix, 'homogeneity')[0, 0]
        haralick_features.append(homo)
        corr = graycoprops(co_matrix, 'correlation')[0, 0]
        haralick_features.append(corr)
        asm = graycoprops(co_matrix, 'ASM')[0, 0]
        haralick_features.append(asm)

    # lecture des fichier csv
    glcmCovid = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/glcmCovid.csv', 'r')
    glcmCovidOpen = csv.reader(glcmCovid)

    glcmFMD = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/glcmFMD.csv', 'r')
    glcmFMDOpen = csv.reader(glcmFMD)

    glcmGLAUCOMA = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/glcmGLAUCOMA.csv', 'r')
    glcmGLAUCOMAOpen = csv.reader(glcmGLAUCOMA)

    glcmKTH = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/glcmKTH.csv', 'r')
    glcmKTHOpen = csv.reader(glcmKTH)

    glcmOUTEX = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/glcmOUTEX.csv', 'r')
    glcmOUTEXOpen = csv.reader(glcmOUTEX)

    n = ""
    if (classe == "COVID"):
        n = 'images'
        o = glcmCovidOpen
    elif (classe == "GLAUCOMA"):
        n = 'GLAUCOMA'
        o = glcmGLAUCOMAOpen
    elif (classe == "KTH"):
        n = 'KTH'
        o = glcmKTHOpen
    elif (classe == "OUTEX"):
        n = "OUTEX"
        o = glcmOUTEXOpen


    listTotal = []
    listTotal2 = []
    if (n != ""):
        for r in o:
            listD = r[0]
            listN = r[1]
            list = r[2:]
            float_list = []

            for item in list:
                float_list.append(float(item))

            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.cityblock(float_list, haralick_features)
            valeur_list = [listD, listN, dist,
                           'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/image/' + n + '/' + listD + '/' + listN]
            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)

        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0

        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        print(nb)
        print(nombre)
        pourc = float(nb) * 100 / float(nombre)

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, pourc]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

    if (classe == "TOUT"):
        listTotal = GLCMMan(img, "1400", "COVID")
        listTotal += GLCMMan(img, "900", "OUTEX")
        listTotal += GLCMMan(img, "500", "GLAUCOMA")
        listTotal += GLCMMan(img, "810", "KTH")


        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0
        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, listTotal[i]['pourcentage']]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

        listTotal2[0]['nbCovid'] = nb
    return listTotal2



def taxonomyMan(img, nombre, classe):
    haralick_features = taxonomy(img)


    taxoCovid = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyCovid.csv', 'r')
    taxoCovidOpen = csv.reader(taxoCovid)

    taxoFMD = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyFMD.csv', 'r')
    taxoFMDOpen = csv.reader(taxoFMD)

    taxoGLAUCOMA = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyGLAUCOMA.csv', 'r')
    taxoGLAUCOMAOpen = csv.reader(taxoGLAUCOMA)

    taxoKTH = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyKTH.csv', 'r')
    taxoKTHOpen = csv.reader(taxoKTH)

    taxoOUTEX = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyOUTEX.csv', 'r')
    taxoOUTEXOpen = csv.reader(taxoOUTEX)

    n = ""
    if (classe == "COVID"):
        n = 'images'
        o = taxoCovidOpen
    elif (classe == "GLAUCOMA"):
        n = 'GLAUCOMA'
        o = taxoGLAUCOMAOpen
    elif (classe == "KTH"):
        n = 'KTH'
        o = taxoKTHOpen
    elif (classe == "OUTEX"):
        n = "OUTEX"
        o = taxoOUTEXOpen


    listTotal = []
    listTotal2 = []
    if (n != ""):
        for r in o:
            listD = r[0]
            listN = r[1]
            list = r[2:]
            float_list = []

            for item in list:
                float_list.append(float(item))

            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.cityblock(float_list, haralick_features)
            valeur_list = [listD, listN, dist,
                           'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/image/' + n + '/' + listD + '/' + listN]
            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)

        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0

        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        print(nb)
        print(nombre)
        pourc = float(nb) * 100 / float(nombre)

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, pourc]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

    if (classe == "TOUT"):
        listTotal = taxonomyMan(img, "1400", "COVID")
        listTotal += taxonomyMan(img, "900", "OUTEX")
        listTotal += taxonomyMan(img, "500", "GLAUCOMA")
        listTotal += taxonomyMan(img, "810", "KTH")


        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0
        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, listTotal[i]['pourcentage']]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

        listTotal2[0]['nbCovid'] = nb

    return listTotal2





def biodiversityEuc(img, nombre, classe):
    haralick_features = biodiversity(img)


    bioCovid = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityCovid.csv', 'r')
    bioCovidOpen = csv.reader(bioCovid)

    bioFMD = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityFMD.csv', 'r')
    bioFMDOpen = csv.reader(bioFMD)

    bioGLAUCOMA = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityGLAUCOMA.csv', 'r')
    bioGLAUCOMAOpen = csv.reader(bioGLAUCOMA)

    bioKTH = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityKTH.csv', 'r')
    bioKTHOpen = csv.reader(bioKTH)

    bioOUTEX = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityOUTEX.csv', 'r')
    bioOUTEXOpen = csv.reader(bioOUTEX)

    n = ""
    if (classe == "COVID"):
        n = 'images'
        o = bioCovidOpen
    elif (classe == "GLAUCOMA"):
        n = 'GLAUCOMA'
        o = bioGLAUCOMAOpen
    elif (classe == "KTH"):
        n = 'KTH'
        o = bioKTHOpen
    elif (classe == "OUTEX"):
        n = "OUTEX"
        o = bioOUTEXOpen


    listTotal = []
    listTotal2 = []
    if (n != ""):
        for r in o:
            listD = r[0]
            listN = r[1]
            list = r[2:]
            float_list = []

            for item in list:
                float_list.append(float(item))

            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.euclidean(float_list, haralick_features)
            valeur_list = [listD, listN, dist,
                           'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/image/' + n + '/' + listD + '/' + listN]
            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)

        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0

        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        print(nb)
        print(nombre)
        pourc = float(nb) * 100 / float(nombre)

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, pourc]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

    if (classe == "TOUT"):
        listTotal = biodiversityEuc(img, "1400", "COVID")
        listTotal += biodiversityEuc(img, "900", "OUTEX")
        listTotal += biodiversityEuc(img, "500", "GLAUCOMA")
        listTotal += biodiversityEuc(img, "810", "KTH")


        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0
        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, listTotal[i]['pourcentage']]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

        listTotal2[0]['nbCovid'] = nb

    return listTotal2


#



def biodiversityCan(img, nombre, classe):
    haralick_features = biodiversity(img)


    bioCovid = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityCovid.csv', 'r')
    bioCovidOpen = csv.reader(bioCovid)

    bioFMD = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityFMD.csv', 'r')
    bioFMDOpen = csv.reader(bioFMD)

    bioGLAUCOMA = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityGLAUCOMA.csv', 'r')
    bioGLAUCOMAOpen = csv.reader(bioGLAUCOMA)

    bioKTH = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityKTH.csv', 'r')
    bioKTHOpen = csv.reader(bioKTH)

    bioOUTEX = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityOUTEX.csv', 'r')
    bioOUTEXOpen = csv.reader(bioOUTEX)

    n = ""
    if (classe == "COVID"):
        n = 'images'
        o = bioCovidOpen
    elif (classe == "GLAUCOMA"):
        n = 'GLAUCOMA'
        o = bioGLAUCOMAOpen
    elif (classe == "KTH"):
        n = 'KTH'
        o = bioKTHOpen
    elif (classe == "OUTEX"):
        n = "OUTEX"
        o = bioOUTEXOpen


    listTotal = []
    listTotal2 = []
    if (n != ""):
        for r in o:
            listD = r[0]
            listN = r[1]
            list = r[2:]
            float_list = []

            for item in list:
                float_list.append(float(item))

            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.canberra(float_list, haralick_features)
            valeur_list = [listD, listN, dist,
                           'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/image/' + n + '/' + listD + '/' + listN]
            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)

        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0

        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        print(nb)
        print(nombre)
        pourc = float(nb) * 100 / float(nombre)

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, pourc]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

    if (classe == "TOUT"):
        listTotal = biodiversityCan(img, "1400", "COVID")
        listTotal += biodiversityCan(img, "900", "OUTEX")
        listTotal += biodiversityCan(img, "500", "GLAUCOMA")
        listTotal += biodiversityCan(img, "810", "KTH")


        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0
        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, listTotal[i]['pourcentage']]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

        listTotal2[0]['nbCovid'] = nb

    return listTotal2






def biodiversityChe(img, nombre, classe):
    haralick_features = biodiversity(img)


    bioCovid = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityCovid.csv', 'r')
    bioCovidOpen = csv.reader(bioCovid)

    bioFMD = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityFMD.csv', 'r')
    bioFMDOpen = csv.reader(bioFMD)

    bioGLAUCOMA = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityGLAUCOMA.csv', 'r')
    bioGLAUCOMAOpen = csv.reader(bioGLAUCOMA)

    bioKTH = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityKTH.csv', 'r')
    bioKTHOpen = csv.reader(bioKTH)

    bioOUTEX = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityOUTEX.csv', 'r')
    bioOUTEXOpen = csv.reader(bioOUTEX)

    n = ""
    if (classe == "COVID"):
        n = 'images'
        o = bioCovidOpen
    elif (classe == "GLAUCOMA"):
        n = 'GLAUCOMA'
        o = bioGLAUCOMAOpen
    elif (classe == "KTH"):
        n = 'KTH'
        o = bioKTHOpen
    elif (classe == "OUTEX"):
        n = "OUTEX"
        o = bioOUTEXOpen

    listTotal = []
    listTotal2 = []
    if (n != ""):
        for r in o:
            listD = r[0]
            listN = r[1]
            list = r[2:]
            float_list = []

            for item in list:
                float_list.append(float(item))

            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.chebyshev(float_list, haralick_features)
            valeur_list = [listD, listN, dist,
                           'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/image/' + n + '/' + listD + '/' + listN]
            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)

        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0

        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        print(nb)
        print(nombre)
        pourc = float(nb) * 100 / float(nombre)

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, pourc]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

    if (classe == "TOUT"):
        listTotal = biodiversityChe(img, "1400", "COVID")
        listTotal += biodiversityChe(img, "900", "OUTEX")
        listTotal += biodiversityChe(img, "500", "GLAUCOMA")
        listTotal += biodiversityChe(img, "810", "KTH")


        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0
        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, listTotal[i]['pourcentage']]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

        listTotal2[0]['nbCovid'] = nb

    return listTotal2





def biodiversityMan(img, nombre, classe):
    haralick_features = biodiversity(img)


    bioCovid = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityCovid.csv', 'r')
    bioCovidOpen = csv.reader(bioCovid)

    bioFMD = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityFMD.csv', 'r')
    bioFMDOpen = csv.reader(bioFMD)

    bioGLAUCOMA = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityGLAUCOMA.csv', 'r')
    bioGLAUCOMAOpen = csv.reader(bioGLAUCOMA)

    bioKTH = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityKTH.csv', 'r')
    bioKTHOpen = csv.reader(bioKTH)

    bioOUTEX = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityOUTEX.csv', 'r')
    bioOUTEXOpen = csv.reader(bioOUTEX)

    n = ""
    if (classe == "COVID"):
        n = 'images'
        o = bioCovidOpen
    elif (classe == "GLAUCOMA"):
        n = 'GLAUCOMA'
        o = bioGLAUCOMAOpen
    elif (classe == "KTH"):
        n = 'KTH'
        o = bioKTHOpen
    elif (classe == "OUTEX"):
        n = "OUTEX"
        o = bioOUTEXOpen


    listTotal = []
    listTotal2 = []
    if (n != ""):
        for r in o:
            listD = r[0]
            listN = r[1]
            list = r[2:]
            float_list = []

            for item in list:
                float_list.append(float(item))

            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.cityblock(float_list, haralick_features)
            valeur_list = [listD, listN, dist,
                           'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/image/' + n + '/' + listD + '/' + listN]
            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)

        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0

        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        print(nb)
        print(nombre)
        pourc = float(nb) * 100 / float(nombre)

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, pourc]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

    if (classe == "TOUT"):
        listTotal = biodiversityMan(img, "1400", "COVID")
        listTotal += biodiversityMan(img, "900", "OUTEX")
        listTotal += biodiversityMan(img, "500", "GLAUCOMA")
        listTotal += biodiversityMan(img, "810", "KTH")


        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0
        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, listTotal[i]['pourcentage']]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

        listTotal2[0]['nbCovid'] = nb

    return listTotal2




def taxonomyEuc(img, nombre, classe):
    haralick_features = taxonomy(img)


    taxoCovid = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyCovid.csv', 'r')
    taxoCovidOpen = csv.reader(taxoCovid)

    taxoFMD = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyFMD.csv', 'r')
    taxoFMDOpen = csv.reader(taxoFMD)

    taxoGLAUCOMA = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyGLAUCOMA.csv', 'r')
    taxoGLAUCOMAOpen = csv.reader(taxoGLAUCOMA)

    taxoKTH = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyKTH.csv', 'r')
    taxoKTHOpen = csv.reader(taxoKTH)

    taxoOUTEX = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyOUTEX.csv', 'r')
    taxoOUTEXOpen = csv.reader(taxoOUTEX)

    n = ""
    if (classe == "COVID"):
        n = 'images'
        o = taxoCovidOpen
    elif (classe == "GLAUCOMA"):
        n = 'GLAUCOMA'
        o = taxoGLAUCOMAOpen
    elif (classe == "KTH"):
        n = 'KTH'
        o = taxoKTHOpen
    elif (classe == "OUTEX"):
        n = "OUTEX"
        o = taxoOUTEXOpen


    listTotal = []
    listTotal2 = []
    if (n != ""):
        for r in o:
            listD = r[0]
            listN = r[1]
            list = r[2:]
            float_list = []

            for item in list:
                float_list.append(float(item))

            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.euclidean(float_list, haralick_features)
            valeur_list = [listD, listN, dist,
                           'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/image/' + n + '/' + listD + '/' + listN]
            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)

        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0

        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        print(nb)
        print(nombre)
        pourc = float(nb) * 100 / float(nombre)

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, pourc]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

    if (classe == "TOUT"):
        listTotal = taxonomyEuc(img, "1400", "COVID")
        listTotal += taxonomyEuc(img, "900", "OUTEX")
        listTotal += taxonomyEuc(img, "500", "GLAUCOMA")
        listTotal += taxonomyEuc(img, "810", "KTH")


        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0
        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, listTotal[i]['pourcentage']]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

        listTotal2[0]['nbCovid'] = nb

    return listTotal2


#




def taxonomyCan(img, nombre, classe):
    haralick_features = taxonomy(img)


    taxoCovid = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyCovid.csv', 'r')
    taxoCovidOpen = csv.reader(taxoCovid)

    taxoFMD = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyFMD.csv', 'r')
    taxoFMDOpen = csv.reader(taxoFMD)

    taxoGLAUCOMA = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyGLAUCOMA.csv', 'r')
    taxoGLAUCOMAOpen = csv.reader(taxoGLAUCOMA)

    taxoKTH = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyKTH.csv', 'r')
    taxoKTHOpen = csv.reader(taxoKTH)

    taxoOUTEX = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyOUTEX.csv', 'r')
    taxoOUTEXOpen = csv.reader(taxoOUTEX)

    n = ""
    if (classe == "COVID"):
        n = 'images'
        o = taxoCovidOpen
    elif (classe == "GLAUCOMA"):
        n = 'GLAUCOMA'
        o = taxoGLAUCOMAOpen
    elif (classe == "KTH"):
        n = 'KTH'
        o = taxoKTHOpen
    elif (classe == "OUTEX"):
        n = "OUTEX"
        o = taxoOUTEXOpen

    listTotal = []
    listTotal2 = []
    if (n != ""):
        for r in o:
            listD = r[0]
            listN = r[1]
            list = r[2:]
            float_list = []

            for item in list:
                float_list.append(float(item))

            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.canberra(float_list, haralick_features)
            valeur_list = [listD, listN, dist,
                           'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/image/' + n + '/' + listD + '/' + listN]
            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)

        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0

        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        print(nb)
        print(nombre)
        pourc = float(nb) * 100 / float(nombre)

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, pourc]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

    if (classe == "TOUT"):
        listTotal = taxonomyCan(img, "1400", "COVID")
        listTotal += taxonomyCan(img, "900", "OUTEX")
        listTotal += taxonomyCan(img, "500", "GLAUCOMA")
        listTotal += taxonomyCan(img, "810", "KTH")


        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0
        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, listTotal[i]['pourcentage']]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

        listTotal2[0]['nbCovid'] = nb

    return listTotal2


#



def taxonomyChe(img, nombre, classe):
    haralick_features = taxonomy(img)


    taxoCovid = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyCovid.csv', 'r')
    taxoCovidOpen = csv.reader(taxoCovid)

    taxoFMD = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyFMD.csv', 'r')
    taxoFMDOpen = csv.reader(taxoFMD)

    taxoGLAUCOMA = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyGLAUCOMA.csv', 'r')
    taxoGLAUCOMAOpen = csv.reader(taxoGLAUCOMA)

    taxoKTH = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyKTH.csv', 'r')
    taxoKTHOpen = csv.reader(taxoKTH)

    taxoOUTEX = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyOUTEX.csv', 'r')
    taxoOUTEXOpen = csv.reader(taxoOUTEX)

    n = ""
    if (classe == "COVID"):
        n = 'images'
        o = taxoCovidOpen
    elif (classe == "GLAUCOMA"):
        n = 'GLAUCOMA'
        o = taxoGLAUCOMAOpen
    elif (classe == "KTH"):
        n = 'KTH'
        o = taxoKTHOpen
    elif (classe == "OUTEX"):
        n = "OUTEX"
        o = taxoOUTEXOpen

    listTotal = []
    listTotal2 = []
    if (n != ""):
        for r in o:
            listD = r[0]
            listN = r[1]
            list = r[2:]
            float_list = []

            for item in list:
                float_list.append(float(item))

            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.chebyshev(float_list, haralick_features)
            valeur_list = [listD, listN, dist,
                           'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/image/' + n + '/' + listD + '/' + listN]
            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)

        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0

        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        print(nb)
        print(nombre)
        pourc = float(nb) * 100 / float(nombre)

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, pourc]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

    if (classe == "TOUT"):
        listTotal = taxonomyChe(img, "1400", "COVID")
        listTotal += taxonomyChe(img, "900", "OUTEX")
        listTotal += taxonomyChe(img, "500", "GLAUCOMA")
        listTotal += taxonomyChe(img, "810", "KTH")


        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0
        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, listTotal[i]['pourcentage']]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

        listTotal2[0]['nbCovid'] = nb

    return listTotal2


#



def biodiversityHaralickEuc(img, nombre, classe):
    def haralick(file):
        # return features.haralick(file)
        haralick_features = features.haralick(file)
        haralick_features = [feat for sublist in haralick_features for feat in sublist]
        return haralick_features  # 52 features

    def BiT(file):
        bit_features = biodiversity(file)
        return bit_features  # 14 features

    haralickk = haralick(img)  # list
    bit_feat = BiT(img)  # list
    haralick_features = haralickk + bit_feat

    taxoCovid = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityHaralickCovid.csv', 'r')
    taxoCovidOpen = csv.reader(taxoCovid)

    taxoFMD = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityHaralickFMD.csv', 'r')
    taxoFMDOpen = csv.reader(taxoFMD)

    taxoGLAUCOMA = open(
        'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityHaralickGLAUCOMA.csv', 'r')
    taxoGLAUCOMAOpen = csv.reader(taxoGLAUCOMA)

    taxoKTH = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityHaralickKTH.csv', 'r')
    taxoKTHOpen = csv.reader(taxoKTH)

    taxoOUTEX = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityHaralickOUTEX.csv', 'r')
    taxoOUTEXOpen = csv.reader(taxoOUTEX)

    n = ""
    if (classe == "COVID"):
        n = 'images'
        o = taxoCovidOpen
    elif (classe == "GLAUCOMA"):
        n = 'GLAUCOMA'
        o = taxoGLAUCOMAOpen
    elif (classe == "KTH"):
        n = 'KTH'
        o = taxoKTHOpen
    elif (classe == "OUTEX"):
        n = "OUTEX"
        o = taxoOUTEXOpen


    listTotal = []
    listTotal2 = []
    if (n != ""):
        for r in o:
            listD = r[0]
            listN = r[1]
            list = r[2:]
            float_list = []

            for item in list:
                float_list.append(float(item))

            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.euclidean(float_list, haralick_features)
            valeur_list = [listD, listN, dist,
                           'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/image/' + n + '/' + listD + '/' + listN]
            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)

        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0

        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        print(nb)
        print(nombre)
        pourc = float(nb) * 100 / float(nombre)

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, pourc]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

    if (classe == "TOUT"):
        listTotal = biodiversityHaralickEuc(img, "1400", "COVID")
        listTotal += biodiversityHaralickEuc(img, "900", "OUTEX")
        listTotal += biodiversityHaralickEuc(img, "500", "GLAUCOMA")
        listTotal += biodiversityHaralickEuc(img, "810", "KTH")


        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0
        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, listTotal[i]['pourcentage']]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

        listTotal2[0]['nbCovid'] = nb

    return listTotal2


#



def biodiversityHaralickChe(img, nombre, classe):
    def haralick(file):
        # return features.haralick(file)
        haralick_features = features.haralick(file)
        haralick_features = [feat for sublist in haralick_features for feat in sublist]
        return haralick_features  # 52 features

    def BiT(file):
        bit_features = biodiversity(file)
        return bit_features  # 14 features

    haralickk = haralick(img)  # list
    bit_feat = BiT(img)  # list
    haralick_features = haralickk + bit_feat

    taxoCovid = open(
        'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityHaralickCovid.csv', 'r')
    taxoCovidOpen = csv.reader(taxoCovid)

    taxoFMD = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityHaralickFMD.csv',
                   'r')
    taxoFMDOpen = csv.reader(taxoFMD)

    taxoGLAUCOMA = open(
        'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityHaralickGLAUCOMA.csv', 'r')
    taxoGLAUCOMAOpen = csv.reader(taxoGLAUCOMA)

    taxoKTH = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityHaralickKTH.csv',
                   'r')
    taxoKTHOpen = csv.reader(taxoKTH)

    taxoOUTEX = open(
        'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityHaralickOUTEX.csv', 'r')
    taxoOUTEXOpen = csv.reader(taxoOUTEX)

    n = ""
    if (classe == "COVID"):
        n = 'images'
        o = taxoCovidOpen
    elif (classe == "GLAUCOMA"):
        n = 'GLAUCOMA'
        o = taxoGLAUCOMAOpen
    elif (classe == "KTH"):
        n = 'KTH'
        o = taxoKTHOpen
    elif (classe == "OUTEX"):
        n = "OUTEX"
        o = taxoOUTEXOpen

    listTotal = []
    listTotal2 = []
    if (n != ""):
        for r in o:
            listD = r[0]
            listN = r[1]
            list = r[2:]
            float_list = []

            for item in list:
                float_list.append(float(item))

            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.chebyshev(float_list, haralick_features)
            valeur_list = [listD, listN, dist,
                           'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/image/' + n + '/' + listD + '/' + listN]
            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)

        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0

        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        print(nb)
        print(nombre)
        pourc = float(nb) * 100 / float(nombre)

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, pourc]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

    if (classe == "TOUT"):
        listTotal = biodiversityHaralickChe(img, "1400", "COVID")
        listTotal += biodiversityHaralickChe(img, "900", "OUTEX")
        listTotal += biodiversityHaralickChe(img, "500", "GLAUCOMA")
        listTotal += biodiversityHaralickChe(img, "810", "KTH")


        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0
        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, listTotal[i]['pourcentage']]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

        listTotal2[0]['nbCovid'] = nb

    return listTotal2




def biodiversityHaralickMan(img, nombre, classe):
    def haralick(file):
        # return features.haralick(file)
        haralick_features = features.haralick(file)
        haralick_features = [feat for sublist in haralick_features for feat in sublist]
        return haralick_features  # 52 features

    def BiT(file):
        bit_features = biodiversity(file)
        return bit_features  # 14 features

    haralickk = haralick(img)  # list
    bit_feat = BiT(img)  # list
    haralick_features = haralickk + bit_feat

    taxoCovid = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityHaralickCovid.csv', 'r')
    taxoCovidOpen = csv.reader(taxoCovid)

    taxoFMD = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityHaralickFMD.csv', 'r')
    taxoFMDOpen = csv.reader(taxoFMD)

    taxoGLAUCOMA = open(
        'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityHaralickGLAUCOMA.csv', 'r')
    taxoGLAUCOMAOpen = csv.reader(taxoGLAUCOMA)

    taxoKTH = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityHaralickKTH.csv', 'r')
    taxoKTHOpen = csv.reader(taxoKTH)

    taxoOUTEX = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityHaralickOUTEX.csv', 'r')
    taxoOUTEXOpen = csv.reader(taxoOUTEX)

    n = ""
    if (classe == "COVID"):
        n = 'images'
        o = taxoCovidOpen
    elif (classe == "GLAUCOMA"):
        n = 'GLAUCOMA'
        o = taxoGLAUCOMAOpen
    elif (classe == "KTH"):
        n = 'KTH'
        o = taxoKTHOpen
    elif (classe == "OUTEX"):
        n = "OUTEX"
        o = taxoOUTEXOpen


    listTotal = []
    listTotal2 = []
    if (n != ""):
        for r in o:
            listD = r[0]
            listN = r[1]
            list = r[2:]
            float_list = []

            for item in list:
                float_list.append(float(item))

            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.cityblock(float_list, haralick_features)
            valeur_list = [listD, listN, dist,
                           'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/image/' + n + '/' + listD + '/' + listN]
            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)

        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0

        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        print(nb)
        print(nombre)
        pourc = float(nb) * 100 / float(nombre)

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, pourc]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

    if (classe == "TOUT"):
        listTotal = biodiversityHaralickMan(img, "1400", "COVID")
        listTotal += biodiversityHaralickMan(img, "900", "OUTEX")
        listTotal += biodiversityHaralickMan(img, "500", "GLAUCOMA")
        listTotal += biodiversityHaralickMan(img, "810", "KTH")


        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0
        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, listTotal[i]['pourcentage']]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

        listTotal2[0]['nbCovid'] = nb

    return listTotal2


#

def biodiversityHaralickCan(img, nombre, classe):
    def haralick(file):
        # return features.haralick(file)
        haralick_features = features.haralick(file)
        haralick_features = [feat for sublist in haralick_features for feat in sublist]
        return haralick_features  # 52 features

    def BiT(file):
        bit_features = biodiversity(file)
        return bit_features  # 14 features

    haralickk = haralick(img)  # list
    bit_feat = BiT(img)  # list
    haralick_features = haralickk + bit_feat

    taxoCovid = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityHaralickCovid.csv', 'r')
    taxoCovidOpen = csv.reader(taxoCovid)

    taxoFMD = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityHaralickFMD.csv', 'r')
    taxoFMDOpen = csv.reader(taxoFMD)

    taxoGLAUCOMA = open(
        'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityHaralickGLAUCOMA.csv', 'r')
    taxoGLAUCOMAOpen = csv.reader(taxoGLAUCOMA)

    taxoKTH = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityHaralickKTH.csv', 'r')
    taxoKTHOpen = csv.reader(taxoKTH)

    taxoOUTEX = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityHaralickOUTEX.csv', 'r')
    taxoOUTEXOpen = csv.reader(taxoOUTEX)

    n = ""
    if (classe == "COVID"):
        n = 'images'
        o = taxoCovidOpen
    elif (classe == "GLAUCOMA"):
        n = 'GLAUCOMA'
        o = taxoGLAUCOMAOpen
    elif (classe == "KTH"):
        n = 'KTH'
        o = taxoKTHOpen
    elif (classe == "OUTEX"):
        n = "OUTEX"
        o = taxoOUTEXOpen

    listTotal = []
    listTotal2 = []
    if (n != ""):
        for r in o:
            listD = r[0]
            listN = r[1]
            list = r[2:]
            float_list = []

            for item in list:
                float_list.append(float(item))

            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.canberra(float_list, haralick_features)
            valeur_list = [listD, listN, dist,
                           'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/image/' + n + '/' + listD + '/' + listN]
            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)

        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0

        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        print(nb)
        print(nombre)
        pourc = float(nb) * 100 / float(nombre)

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, pourc]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

    if (classe == "TOUT"):
        listTotal = biodiversityHaralickCan(img, "1400", "COVID")
        listTotal += biodiversityHaralickCan(img, "900", "OUTEX")
        listTotal += biodiversityHaralickCan(img, "500", "GLAUCOMA")
        listTotal += biodiversityHaralickCan(img, "810", "KTH")


        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0
        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, listTotal[i]['pourcentage']]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

        listTotal2[0]['nbCovid'] = nb

    return listTotal2


def biodiversityGLCMEuc(img, nombre, classe):
    def glcm(file):
        # skimage.feature.texture.greycomatrix(image, distances, angles, levels=256, symmetric=False, normed=False)
        features = []
        angles = [0, np.pi / 2, np.pi / 4]
        for angle in angles:
            print('Angle: ', angle)
            co_matrix = graycomatrix(file, [5], [angle], levels=256, symmetric=True, normed=True)
            diss = graycoprops(co_matrix, 'dissimilarity')[0, 0]
            features.append(diss)
            cont = graycoprops(co_matrix, 'contrast')[0, 0]
            features.append(cont)
            ener = graycoprops(co_matrix, 'energy')[0, 0]
            features.append(ener)
            homo = graycoprops(co_matrix, 'homogeneity')[0, 0]
            features.append(homo)
            corr = graycoprops(co_matrix, 'correlation')[0, 0]
            features.append(corr)
            asm = graycoprops(co_matrix, 'ASM')[0, 0]
            features.append(asm)
        return features

    def BiT(file):
        bit_features = biodiversity(file)
        return bit_features  # 14 features

    glcmk = glcm(img)  # list
    bit_feat = BiT(img)  # list
    haralick_features = glcmk + bit_feat

    taxoCovid = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityGlcmCovid.csv',
                     'r')
    taxoCovidOpen = csv.reader(taxoCovid)

    ds = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityGlcmFMD3.csv', 'r')
    dsOpen = csv.reader(ds)

    taxoGLAUCOMA = open(
        'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityGlcmGLAUCOMA.csv', 'r')
    taxoGLAUCOMAOpen = csv.reader(taxoGLAUCOMA)

    taxoKTH = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityGlcmKTH.csv', 'r')
    taxoKTHOpen = csv.reader(taxoKTH)

    taxoOUTEX = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityGlcmOUTEX.csv',
                     'r')
    taxoOUTEXOpen = csv.reader(taxoOUTEX)

    n = ""
    if (classe == "COVID"):
        n = 'images'
        o = taxoCovidOpen
    elif (classe == "GLAUCOMA"):
        n = 'GLAUCOMA'
        o = taxoGLAUCOMAOpen
    elif (classe == "KTH"):
        n = 'KTH'
        o = taxoKTHOpen
    elif (classe == "OUTEX"):
        n = "OUTEX"
        o = taxoOUTEXOpen


    listTotal = []
    listTotal2 = []
    if (n != ""):
        for r in o:
            listD = r[0]
            listN = r[1]
            list = r[2:]
            float_list = []

            for item in list:
                float_list.append(float(item))

            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.euclidean(float_list, haralick_features)
            valeur_list = [listD, listN, dist,
                           'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/image/' + n + '/' + listD + '/' + listN]
            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)

        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0

        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        print(nb)
        print(nombre)
        pourc = float(nb) * 100 / float(nombre)

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, pourc]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

    if (classe == "TOUT"):
        listTotal = biodiversityGLCMEuc(img, "1400", "COVID")
        listTotal += biodiversityGLCMEuc(img, "900", "OUTEX")
        listTotal += biodiversityGLCMEuc(img, "500", "GLAUCOMA")
        listTotal += biodiversityGLCMEuc(img, "810", "KTH")

        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0
        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, listTotal[i]['pourcentage']]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

        listTotal2[0]['nbCovid'] = nb

    return listTotal2


def biodiversityGLCMCan(img, nombre, classe):

    def glcm(file):
        # skimage.feature.texture.greycomatrix(image, distances, angles, levels=256, symmetric=False, normed=False)
        features = []
        angles = [0, np.pi / 2, np.pi / 4]
        for angle in angles:
            print('Angle: ', angle)
            co_matrix = graycomatrix(file, [5], [angle], levels=256, symmetric=True, normed=True)
            diss = graycoprops(co_matrix, 'dissimilarity')[0, 0]
            features.append(diss)
            cont = graycoprops(co_matrix, 'contrast')[0, 0]
            features.append(cont)
            ener = graycoprops(co_matrix, 'energy')[0, 0]
            features.append(ener)
            homo = graycoprops(co_matrix, 'homogeneity')[0, 0]
            features.append(homo)
            corr = graycoprops(co_matrix, 'correlation')[0, 0]
            features.append(corr)
            asm = graycoprops(co_matrix, 'ASM')[0, 0]
            features.append(asm)
        return features

    def BiT(file):
        bit_features = biodiversity(file)
        return bit_features  # 14 features

    glcmk = glcm(img)  # list
    bit_feat = BiT(img)  # list
    haralick_features = glcmk + bit_feat

    taxoCovid = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityGlcmCovid.csv', 'r')
    taxoCovidOpen = csv.reader(taxoCovid)

    taxoFMD = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityGlcmFMD3.csv', 'r')
    taxoFMDOpen = csv.reader(taxoFMD)

    taxoGLAUCOMA = open(
        'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityGlcmGLAUCOMA.csv', 'r')
    taxoGLAUCOMAOpen = csv.reader(taxoGLAUCOMA)

    taxoKTH = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityGlcmKTH.csv', 'r')
    taxoKTHOpen = csv.reader(taxoKTH)

    taxoOUTEX = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityGlcmOUTEX.csv', 'r')
    taxoOUTEXOpen = csv.reader(taxoOUTEX)

    n = ""
    if (classe == "COVID"):
        n = 'images'
        o = taxoCovidOpen
    elif (classe == "GLAUCOMA"):
        n = 'GLAUCOMA'
        o = taxoGLAUCOMAOpen
    elif (classe == "KTH"):
        n = 'KTH'
        o = taxoKTHOpen
    elif (classe == "OUTEX"):
        n = "OUTEX"
        o = taxoOUTEXOpen

    listTotal = []
    listTotal2 = []
    if (n != ""):
        for r in o:
            listD = r[0]
            listN = r[1]
            list = r[2:]
            float_list = []

            for item in list:
                float_list.append(float(item))

            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.canberra(float_list, haralick_features)
            valeur_list = [listD, listN, dist,
                           'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/image/' + n + '/' + listD + '/' + listN]
            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)

        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0

        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        print(nb)
        print(nombre)
        pourc = float(nb) * 100 / float(nombre)

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, pourc]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

    if (classe == "TOUT"):
        listTotal = biodiversityGLCMCan(img, "1400", "COVID")
        listTotal += biodiversityGLCMCan(img, "900", "OUTEX")
        listTotal += biodiversityGLCMCan(img, "500", "GLAUCOMA")
        listTotal += biodiversityGLCMCan(img, "810", "KTH")
        

        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0
        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, listTotal[i]['pourcentage']]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

        listTotal2[0]['nbCovid'] = nb

    return listTotal2


#



def biodiversityGLCMChe(img, nombre, classe):

    def glcm(file):
        # skimage.feature.texture.greycomatrix(image, distances, angles, levels=256, symmetric=False, normed=False)
        features = []
        angles = [0, np.pi / 2, np.pi / 4]
        for angle in angles:
            print('Angle: ', angle)
            co_matrix = graycomatrix(file, [5], [angle], levels=256, symmetric=True, normed=True)
            diss = graycoprops(co_matrix, 'dissimilarity')[0, 0]
            features.append(diss)
            cont = graycoprops(co_matrix, 'contrast')[0, 0]
            features.append(cont)
            ener = graycoprops(co_matrix, 'energy')[0, 0]
            features.append(ener)
            homo = graycoprops(co_matrix, 'homogeneity')[0, 0]
            features.append(homo)
            corr = graycoprops(co_matrix, 'correlation')[0, 0]
            features.append(corr)
            asm = graycoprops(co_matrix, 'ASM')[0, 0]
            features.append(asm)
        return features

    def BiT(file):
        bit_features = biodiversity(file)
        return bit_features  # 14 features

    glcmk = glcm(img)  # list
    bit_feat = BiT(img)  # list
    haralick_features = glcmk + bit_feat

    taxoCovid = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityGlcmCovid.csv', 'r')
    taxoCovidOpen = csv.reader(taxoCovid)

    taxoFMD = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityGlcmFMD3.csv', 'r')
    taxoFMDOpen = csv.reader(taxoFMD)

    taxoGLAUCOMA = open(
        'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityGlcmGLAUCOMA.csv', 'r')
    taxoGLAUCOMAOpen = csv.reader(taxoGLAUCOMA)

    taxoKTH = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityGlcmKTH.csv', 'r')
    taxoKTHOpen = csv.reader(taxoKTH)

    taxoOUTEX = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityGlcmOUTEX.csv', 'r')
    taxoOUTEXOpen = csv.reader(taxoOUTEX)

    n = ""
    if (classe == "COVID"):
        n = 'images'
        o = taxoCovidOpen
    elif (classe == "GLAUCOMA"):
        n = 'GLAUCOMA'
        o = taxoGLAUCOMAOpen
    elif (classe == "KTH"):
        n = 'KTH'
        o = taxoKTHOpen
    elif (classe == "OUTEX"):
        n = "OUTEX"
        o = taxoOUTEXOpen


    listTotal = []
    listTotal2 = []
    if (n != ""):
        for r in o:
            listD = r[0]
            listN = r[1]
            list = r[2:]
            float_list = []

            for item in list:
                float_list.append(float(item))

            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.chebyshev(float_list, haralick_features)
            valeur_list = [listD, listN, dist,
                           'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/image/' + n + '/' + listD + '/' + listN]
            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)

        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0

        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        print(nb)
        print(nombre)
        pourc = float(nb) * 100 / float(nombre)

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, pourc]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

    if (classe == "TOUT"):
        listTotal = biodiversityGLCMChe(img, "1400", "COVID")
        listTotal += biodiversityGLCMChe(img, "900", "OUTEX")
        listTotal += biodiversityGLCMChe(img, "500", "GLAUCOMA")
        listTotal += biodiversityGLCMChe(img, "810", "KTH")


        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0
        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, listTotal[i]['pourcentage']]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

        listTotal2[0]['nbCovid'] = nb

    return listTotal2


#


def biodiversityGLCMMan(img, nombre, classe):

    def glcm(file):
        # skimage.feature.texture.greycomatrix(image, distances, angles, levels=256, symmetric=False, normed=False)
        features = []
        angles = [0, np.pi / 2, np.pi / 4]
        for angle in angles:
            print('Angle: ', angle)
            co_matrix = graycomatrix(file, [5], [angle], levels=256, symmetric=True, normed=True)
            diss = graycoprops(co_matrix, 'dissimilarity')[0, 0]
            features.append(diss)
            cont = graycoprops(co_matrix, 'contrast')[0, 0]
            features.append(cont)
            ener = graycoprops(co_matrix, 'energy')[0, 0]
            features.append(ener)
            homo = graycoprops(co_matrix, 'homogeneity')[0, 0]
            features.append(homo)
            corr = graycoprops(co_matrix, 'correlation')[0, 0]
            features.append(corr)
            asm = graycoprops(co_matrix, 'ASM')[0, 0]
            features.append(asm)
        return features

    def BiT(file):
        bit_features = biodiversity(file)
        return bit_features  # 14 features

    glcmk = glcm(img)  # list
    bit_feat = BiT(img)  # list
    haralick_features = glcmk + bit_feat

    taxoCovid = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityGlcmCovid.csv', 'r')
    taxoCovidOpen = csv.reader(taxoCovid)

    taxoFMD = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityGlcmFMD3.csv', 'r')
    taxoFMDOpen = csv.reader(taxoFMD)

    taxoGLAUCOMA = open(
        'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityGlcmGLAUCOMA.csv', 'r')
    taxoGLAUCOMAOpen = csv.reader(taxoGLAUCOMA)

    taxoKTH = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityGlcmKTH.csv', 'r')
    taxoKTHOpen = csv.reader(taxoKTH)

    taxoOUTEX = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/biodiversityGlcmOUTEX.csv', 'r')
    taxoOUTEXOpen = csv.reader(taxoOUTEX)

    n = ""
    if (classe == "COVID"):
        n = 'images'
        o = taxoCovidOpen
    elif (classe == "GLAUCOMA"):
        n = 'GLAUCOMA'
        o = taxoGLAUCOMAOpen
    elif (classe == "KTH"):
        n = 'KTH'
        o = taxoKTHOpen
    elif (classe == "OUTEX"):
        n = "OUTEX"
        o = taxoOUTEXOpen

    listTotal = []
    listTotal2 = []
    if (n != ""):
        for r in o:
            listD = r[0]
            listN = r[1]
            list = r[2:]
            float_list = []

            for item in list:
                float_list.append(float(item))

            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.cityblock(float_list, haralick_features)
            valeur_list = [listD, listN, dist,
                           'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/image/' + n + '/' + listD + '/' + listN]
            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)

        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0

        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        print(nb)
        print(nombre)
        pourc = float(nb) * 100 / float(nombre)

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, pourc]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

    if (classe == "TOUT"):
        listTotal = biodiversityGLCMMan(img, "1400", "COVID")
        listTotal += biodiversityGLCMMan(img, "900", "OUTEX")
        listTotal += biodiversityGLCMMan(img, "500", "GLAUCOMA")
        listTotal += biodiversityGLCMMan(img, "810", "KTH")


        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0
        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, listTotal[i]['pourcentage']]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

        listTotal2[0]['nbCovid'] = nb

    return listTotal2

#




def taxonomyGLCMEuc(img, nombre, classe):
    def glcm(file):
        # skimage.feature.texture.greycomatrix(image, distances, angles, levels=256, symmetric=False, normed=False)
        features = []
        angles = [0, np.pi / 2, np.pi / 4]
        for angle in angles:
            print('Angle: ', angle)
            co_matrix = graycomatrix(file, [5], [angle], levels=256, symmetric=True, normed=True)
            diss = graycoprops(co_matrix, 'dissimilarity')[0, 0]
            features.append(diss)
            cont = graycoprops(co_matrix, 'contrast')[0, 0]
            features.append(cont)
            ener = graycoprops(co_matrix, 'energy')[0, 0]
            features.append(ener)
            homo = graycoprops(co_matrix, 'homogeneity')[0, 0]
            features.append(homo)
            corr = graycoprops(co_matrix, 'correlation')[0, 0]
            features.append(corr)
            asm = graycoprops(co_matrix, 'ASM')[0, 0]
            features.append(asm)
        return features

    def BiT(file):
        bit_features = taxonomy(file)
        return bit_features  # 14 features

    glcmk = glcm(img)  # list
    bit_feat = BiT(img)  # list
    haralick_features = glcmk + bit_feat

    taxoCovid = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyGlcmCovid.csv', 'r')
    taxoCovidOpen = csv.reader(taxoCovid)

    taxoFMD = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyGlcmFMD.csv', 'r')
    taxoFMDOpen = csv.reader(taxoFMD)

    taxoGLAUCOMA = open(
        'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyGlcmGLAUCOMA.csv', 'r')
    taxoGLAUCOMAOpen = csv.reader(taxoGLAUCOMA)

    taxoKTH = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyGlcmKTH.csv', 'r')
    taxoKTHOpen = csv.reader(taxoKTH)

    taxoOUTEX = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyGlcmOUTEX.csv', 'r')
    taxoOUTEXOpen = csv.reader(taxoOUTEX)

    n = ""
    if (classe == "COVID"):
        n = 'images'
        o = taxoCovidOpen
    elif (classe == "GLAUCOMA"):
        n = 'GLAUCOMA'
        o = taxoGLAUCOMAOpen
    elif (classe == "KTH"):
        n = 'KTH'
        o = taxoKTHOpen
    elif (classe == "OUTEX"):
        n = "OUTEX"
        o = taxoOUTEXOpen

    listTotal = []
    listTotal2 = []
    if (n != ""):
        for r in o:
            listD = r[0]
            listN = r[1]
            list = r[2:]
            float_list = []

            for item in list:
                float_list.append(float(item))

            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.euclidean(float_list, haralick_features)
            valeur_list = [listD, listN, dist,
                           'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/image/' + n + '/' + listD + '/' + listN]
            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)

        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0

        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        print(nb)
        print(nombre)
        pourc = float(nb) * 100 / float(nombre)

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, pourc]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

    if (classe == "TOUT"):
        listTotal = taxonomyGLCMEuc(img, "1400", "COVID")
        listTotal += taxonomyGLCMEuc(img, "900", "OUTEX")
        listTotal += taxonomyGLCMEuc(img, "500", "GLAUCOMA")
        listTotal += taxonomyGLCMEuc(img, "810", "KTH")


        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0
        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, listTotal[i]['pourcentage']]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

        listTotal2[0]['nbCovid'] = nb

    return listTotal2


#

def taxonomyGLCMCan(img, nombre, classe):

    def glcm(file):
        # skimage.feature.texture.greycomatrix(image, distances, angles, levels=256, symmetric=False, normed=False)
        features = []
        angles = [0, np.pi / 2, np.pi / 4]
        for angle in angles:
            print('Angle: ', angle)
            co_matrix = graycomatrix(file, [5], [angle], levels=256, symmetric=True, normed=True)
            diss = graycoprops(co_matrix, 'dissimilarity')[0, 0]
            features.append(diss)
            cont = graycoprops(co_matrix, 'contrast')[0, 0]
            features.append(cont)
            ener = graycoprops(co_matrix, 'energy')[0, 0]
            features.append(ener)
            homo = graycoprops(co_matrix, 'homogeneity')[0, 0]
            features.append(homo)
            corr = graycoprops(co_matrix, 'correlation')[0, 0]
            features.append(corr)
            asm = graycoprops(co_matrix, 'ASM')[0, 0]
            features.append(asm)
        return features

    def BiT(file):
        bit_features = taxonomy(file)
        return bit_features  # 14 features

    glcmk = glcm(img)  # list
    bit_feat = BiT(img)  # list
    haralick_features = glcmk + bit_feat

    taxoCovid = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyGlcmCovid.csv', 'r')
    taxoCovidOpen = csv.reader(taxoCovid)

    taxoFMD = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyGlcmFMD.csv', 'r')
    taxoFMDOpen = csv.reader(taxoFMD)

    taxoGLAUCOMA = open(
        'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyGlcmGLAUCOMA.csv', 'r')
    taxoGLAUCOMAOpen = csv.reader(taxoGLAUCOMA)

    taxoKTH = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyGlcmKTH.csv', 'r')
    taxoKTHOpen = csv.reader(taxoKTH)

    taxoOUTEX = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyGlcmOUTEX.csv', 'r')
    taxoOUTEXOpen = csv.reader(taxoOUTEX)

    n = ""
    if (classe == "COVID"):
        n = 'images'
        o = taxoCovidOpen
    elif (classe == "GLAUCOMA"):
        n = 'GLAUCOMA'
        o = taxoGLAUCOMAOpen
    elif (classe == "KTH"):
        n = 'KTH'
        o = taxoKTHOpen
    elif (classe == "OUTEX"):
        n = "OUTEX"
        o = taxoOUTEXOpen

    listTotal = []
    listTotal2 = []
    if (n != ""):
        for r in o:
            listD = r[0]
            listN = r[1]
            list = r[2:]
            float_list = []

            for item in list:
                float_list.append(float(item))

            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.canberra(float_list, haralick_features)
            valeur_list = [listD, listN, dist,
                           'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/image/' + n + '/' + listD + '/' + listN]
            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)

        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0

        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        print(nb)
        print(nombre)
        pourc = float(nb) * 100 / float(nombre)

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, pourc]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

    if (classe == "TOUT"):
        listTotal = taxonomyGLCMCan(img, "1400", "COVID")
        listTotal += taxonomyGLCMCan(img, "900", "OUTEX")
        listTotal += taxonomyGLCMCan(img, "500", "GLAUCOMA")
        listTotal += taxonomyGLCMCan(img, "810", "KTH")


        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0
        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, listTotal[i]['pourcentage']]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

        listTotal2[0]['nbCovid'] = nb

    return listTotal2


#


def taxonomyGLCMChe(img, nombre, classe):

    def glcm(file):
        # skimage.feature.texture.greycomatrix(image, distances, angles, levels=256, symmetric=False, normed=False)
        features = []
        angles = [0, np.pi / 2, np.pi / 4]
        for angle in angles:
            print('Angle: ', angle)
            co_matrix = graycomatrix(file, [5], [angle], levels=256, symmetric=True, normed=True)
            diss = graycoprops(co_matrix, 'dissimilarity')[0, 0]
            features.append(diss)
            cont = graycoprops(co_matrix, 'contrast')[0, 0]
            features.append(cont)
            ener = graycoprops(co_matrix, 'energy')[0, 0]
            features.append(ener)
            homo = graycoprops(co_matrix, 'homogeneity')[0, 0]
            features.append(homo)
            corr = graycoprops(co_matrix, 'correlation')[0, 0]
            features.append(corr)
            asm = graycoprops(co_matrix, 'ASM')[0, 0]
            features.append(asm)
        return features

    def BiT(file):
        bit_features = taxonomy(file)
        return bit_features  # 14 features

    glcmk = glcm(img)  # list
    bit_feat = BiT(img)  # list
    haralick_features = glcmk + bit_feat

    taxoCovid = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyGlcmCovid.csv', 'r')
    taxoCovidOpen = csv.reader(taxoCovid)

    taxoFMD = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyGlcmFMD.csv', 'r')
    taxoFMDOpen = csv.reader(taxoFMD)

    taxoGLAUCOMA = open(
        'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyGlcmGLAUCOMA.csv', 'r')
    taxoGLAUCOMAOpen = csv.reader(taxoGLAUCOMA)

    taxoKTH = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyGlcmKTH.csv', 'r')
    taxoKTHOpen = csv.reader(taxoKTH)

    taxoOUTEX = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyGlcmOUTEX.csv', 'r')
    taxoOUTEXOpen = csv.reader(taxoOUTEX)

    n = ""
    if (classe == "COVID"):
        n = 'images'
        o = taxoCovidOpen
    elif (classe == "GLAUCOMA"):
        n = 'GLAUCOMA'
        o = taxoGLAUCOMAOpen
    elif (classe == "KTH"):
        n = 'KTH'
        o = taxoKTHOpen
    elif (classe == "OUTEX"):
        n = "OUTEX"
        o = taxoOUTEXOpen


    listTotal = []
    listTotal2 = []
    if (n != ""):
        for r in o:
            listD = r[0]
            listN = r[1]
            list = r[2:]
            float_list = []

            for item in list:
                float_list.append(float(item))

            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.chebyshev(float_list, haralick_features)
            valeur_list = [listD, listN, dist,
                           'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/image/' + n + '/' + listD + '/' + listN]
            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)

        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0

        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        print(nb)
        print(nombre)
        pourc = float(nb) * 100 / float(nombre)

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, pourc]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

    if (classe == "TOUT"):
        listTotal = taxonomyGLCMChe(img, "1400", "COVID")
        listTotal += taxonomyGLCMChe(img, "900", "OUTEX")
        listTotal += taxonomyGLCMChe(img, "500", "GLAUCOMA")
        listTotal += taxonomyGLCMChe(img, "810", "KTH")


        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0
        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, listTotal[i]['pourcentage']]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

        listTotal2[0]['nbCovid'] = nb

    return listTotal2




#

def taxonomyGLCMMan(img, nombre, classe):

    def glcm(file):
        # skimage.feature.texture.greycomatrix(image, distances, angles, levels=256, symmetric=False, normed=False)
        features = []
        angles = [0, np.pi / 2, np.pi / 4]
        for angle in angles:
            print('Angle: ', angle)
            co_matrix = graycomatrix(file, [5], [angle], levels=256, symmetric=True, normed=True)
            diss = graycoprops(co_matrix, 'dissimilarity')[0, 0]
            features.append(diss)
            cont = graycoprops(co_matrix, 'contrast')[0, 0]
            features.append(cont)
            ener = graycoprops(co_matrix, 'energy')[0, 0]
            features.append(ener)
            homo = graycoprops(co_matrix, 'homogeneity')[0, 0]
            features.append(homo)
            corr = graycoprops(co_matrix, 'correlation')[0, 0]
            features.append(corr)
            asm = graycoprops(co_matrix, 'ASM')[0, 0]
            features.append(asm)
        return features

    def BiT(file):
        bit_features = taxonomy(file)
        return bit_features  # 14 features

    glcmk = glcm(img)  # list
    bit_feat = BiT(img)  # list
    haralick_features = glcmk + bit_feat

    taxoCovid = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyGlcmCovid.csv', 'r')
    taxoCovidOpen = csv.reader(taxoCovid)

    taxoFMD = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyGlcmFMD.csv', 'r')
    taxoFMDOpen = csv.reader(taxoFMD)

    taxoGLAUCOMA = open(
        'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyGlcmGLAUCOMA.csv', 'r')
    taxoGLAUCOMAOpen = csv.reader(taxoGLAUCOMA)

    taxoKTH = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyGlcmKTH.csv', 'r')
    taxoKTHOpen = csv.reader(taxoKTH)

    taxoOUTEX = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyGlcmOUTEX.csv', 'r')
    taxoOUTEXOpen = csv.reader(taxoOUTEX)

    n = ""
    if (classe == "COVID"):
        n = 'images'
        o = taxoCovidOpen
    elif (classe == "GLAUCOMA"):
        n = 'GLAUCOMA'
        o = taxoGLAUCOMAOpen
    elif (classe == "KTH"):
        n = 'KTH'
        o = taxoKTHOpen
    elif (classe == "OUTEX"):
        n = "OUTEX"
        o = taxoOUTEXOpen

    listTotal = []
    listTotal2 = []
    if (n != ""):
        for r in o:
            listD = r[0]
            listN = r[1]
            list = r[2:]
            float_list = []

            for item in list:
                float_list.append(float(item))

            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.cityblock(float_list, haralick_features)
            valeur_list = [listD, listN, dist,
                           'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/image/' + n + '/' + listD + '/' + listN]
            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)

        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0

        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        print(nb)
        print(nombre)
        pourc = float(nb) * 100 / float(nombre)

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, pourc]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

    if (classe == "TOUT"):
        listTotal = taxonomyGLCMMan(img, "1400", "COVID")
        listTotal += taxonomyGLCMMan(img, "900", "OUTEX")
        listTotal += taxonomyGLCMMan(img, "500", "GLAUCOMA")
        listTotal += taxonomyGLCMMan(img, "810", "KTH")


        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0
        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, listTotal[i]['pourcentage']]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

        listTotal2[0]['nbCovid'] = nb

    return listTotal2

#


def taxonomyHaralickEuc(img, nombre, classe):

    def haralick(file):
        # return features.haralick(file)
        haralick_features = features.haralick(file)
        haralick_features = [feat for sublist in haralick_features for feat in sublist]
        return haralick_features  # 52 features

    def BiT(file):
        bit_features = taxonomy(file)
        return bit_features  # 14 features

    Haralickk = haralick(img)  # list
    bit_feat = BiT(img)  # list
    haralick_features = Haralickk + bit_feat

    taxoCovid = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyHaralickCovid.csv', 'r')
    taxoCovidOpen = csv.reader(taxoCovid)

    taxoFMD = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyHaralickFMD.csv', 'r')
    taxoFMDOpen = csv.reader(taxoFMD)

    taxoGLAUCOMA = open(
        'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyHaralickGLAUCOMA.csv', 'r')
    taxoGLAUCOMAOpen = csv.reader(taxoGLAUCOMA)

    taxoKTH = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyHaralickKTH.csv', 'r')
    taxoKTHOpen = csv.reader(taxoKTH)

    taxoOUTEX = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyHaralickOUTEX.csv', 'r')
    taxoOUTEXOpen = csv.reader(taxoOUTEX)

    n = ""
    if (classe == "COVID"):
        n = 'images'
        o = taxoCovidOpen
    elif (classe == "GLAUCOMA"):
        n = 'GLAUCOMA'
        o = taxoGLAUCOMAOpen
    elif (classe == "KTH"):
        n = 'KTH'
        o = taxoKTHOpen
    elif (classe == "OUTEX"):
        n = "OUTEX"
        o = taxoOUTEXOpen

    listTotal = []
    listTotal2 = []
    if (n != ""):
        for r in o:
            listD = r[0]
            listN = r[1]
            list = r[2:]
            float_list = []

            for item in list:
                float_list.append(float(item))

            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.euclidean(float_list, haralick_features)
            valeur_list = [listD, listN, dist,
                           'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/image/' + n + '/' + listD + '/' + listN]
            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)

        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0

        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        print(nb)
        print(nombre)
        pourc = float(nb) * 100 / float(nombre)

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, pourc]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

    if (classe == "TOUT"):
        listTotal = taxonomyHaralickEuc(img, "1400", "COVID")
        listTotal += taxonomyHaralickEuc(img, "900", "OUTEX")
        listTotal += taxonomyHaralickEuc(img, "500", "GLAUCOMA")
        listTotal += taxonomyHaralickEuc(img, "810", "KTH")


        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0
        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, listTotal[i]['pourcentage']]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

        listTotal2[0]['nbCovid'] = nb

    return listTotal2



def taxonomyHaralickCan(img, nombre, classe):

    def haralick(file):
        # return features.haralick(file)
        haralick_features = features.haralick(file)
        haralick_features = [feat for sublist in haralick_features for feat in sublist]
        return haralick_features  # 52 features

    def BiT(file):
        bit_features = taxonomy(file)
        return bit_features  # 14 features

    Haralickk = haralick(img)  # list
    bit_feat = BiT(img)  # list
    haralick_features = Haralickk + bit_feat

    taxoCovid = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyHaralickCovid.csv', 'r')
    taxoCovidOpen = csv.reader(taxoCovid)

    taxoFMD = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyHaralickFMD.csv', 'r')
    taxoFMDOpen = csv.reader(taxoFMD)

    taxoGLAUCOMA = open(
        'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyHaralickGLAUCOMA.csv', 'r')
    taxoGLAUCOMAOpen = csv.reader(taxoGLAUCOMA)

    taxoKTH = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyHaralickKTH.csv', 'r')
    taxoKTHOpen = csv.reader(taxoKTH)

    taxoOUTEX = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyHaralickOUTEX.csv', 'r')
    taxoOUTEXOpen = csv.reader(taxoOUTEX)

    n = ""
    if (classe == "COVID"):
        n = 'images'
        o = taxoCovidOpen
    elif (classe == "GLAUCOMA"):
        n = 'GLAUCOMA'
        o = taxoGLAUCOMAOpen
    elif (classe == "KTH"):
        n = 'KTH'
        o = taxoKTHOpen
    elif (classe == "OUTEX"):
        n = "OUTEX"
        o = taxoOUTEXOpen

    listTotal = []
    listTotal2 = []
    if (n != ""):
        for r in o:
            listD = r[0]
            listN = r[1]
            list = r[2:]
            float_list = []

            for item in list:
                float_list.append(float(item))

            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.canberra(float_list, haralick_features)
            valeur_list = [listD, listN, dist,
                           'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/image/' + n + '/' + listD + '/' + listN]
            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)

        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0

        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        print(nb)
        print(nombre)
        pourc = float(nb) * 100 / float(nombre)

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, pourc]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

    if (classe == "TOUT"):
        listTotal = taxonomyHaralickCan(img, "1400", "COVID")
        listTotal += taxonomyHaralickCan(img, "900", "OUTEX")
        listTotal += taxonomyHaralickCan(img, "500", "GLAUCOMA")
        listTotal += taxonomyHaralickCan(img, "810", "KTH")

        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0
        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, listTotal[i]['pourcentage']]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

        listTotal2[0]['nbCovid'] = nb

    return listTotal2


def taxonomyHaralickChe(img, nombre, classe):

    def haralick(file):
        # return features.haralick(file)
        haralick_features = features.haralick(file)
        haralick_features = [feat for sublist in haralick_features for feat in sublist]
        return haralick_features  # 52 features

    def BiT(file):
        bit_features = taxonomy(file)
        return bit_features  # 14 features

    Haralickk = haralick(img)  # list
    bit_feat = BiT(img)  # list
    haralick_features = Haralickk + bit_feat

    taxoCovid = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyHaralickCovid.csv', 'r')
    taxoCovidOpen = csv.reader(taxoCovid)

    taxoFMD = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyHaralickFMD.csv', 'r')
    taxoFMDOpen = csv.reader(taxoFMD)

    taxoGLAUCOMA = open(
        'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyHaralickGLAUCOMA.csv', 'r')
    taxoGLAUCOMAOpen = csv.reader(taxoGLAUCOMA)

    taxoKTH = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyHaralickKTH.csv', 'r')
    taxoKTHOpen = csv.reader(taxoKTH)

    taxoOUTEX = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyHaralickOUTEX.csv', 'r')
    taxoOUTEXOpen = csv.reader(taxoOUTEX)

    n = ""
    if (classe == "COVID"):
        n = 'images'
        o = taxoCovidOpen
    elif (classe == "GLAUCOMA"):
        n = 'GLAUCOMA'
        o = taxoGLAUCOMAOpen
    elif (classe == "KTH"):
        n = 'KTH'
        o = taxoKTHOpen
    elif (classe == "OUTEX"):
        n = "OUTEX"
        o = taxoOUTEXOpen

    listTotal = []
    listTotal2 = []
    if (n != ""):
        for r in o:
            listD = r[0]
            listN = r[1]
            list = r[2:]
            float_list = []

            for item in list:
                float_list.append(float(item))

            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.chebyshev(float_list, haralick_features)
            valeur_list = [listD, listN, dist,
                           'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/image/' + n + '/' + listD + '/' + listN]
            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)

        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0

        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        print(nb)
        print(nombre)
        pourc = float(nb) * 100 / float(nombre)

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, pourc]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

    if (classe == "TOUT"):
        listTotal = taxonomyHaralickChe(img, "1400", "COVID")
        listTotal += taxonomyHaralickChe(img, "900", "OUTEX")
        listTotal += taxonomyHaralickChe(img, "500", "GLAUCOMA")
        listTotal += taxonomyHaralickChe(img, "810", "KTH")

        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0
        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, listTotal[i]['pourcentage']]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

        listTotal2[0]['nbCovid'] = nb

    return listTotal2





def taxonomyHaralickMan(img, nombre, classe):
    def haralick(file):
        # return features.haralick(file)
        haralick_features = features.haralick(file)
        haralick_features = [feat for sublist in haralick_features for feat in sublist]
        return haralick_features  # 52 features

    def BiT(file):
        bit_features = taxonomy(file)
        return bit_features  # 14 features

    Haralickk = haralick(img)  # list
    bit_feat = BiT(img)  # list
    haralick_features = Haralickk + bit_feat

    taxoCovid = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyHaralickCovid.csv', 'r')
    taxoCovidOpen = csv.reader(taxoCovid)

    taxoFMD = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyHaralickFMD.csv', 'r')
    taxoFMDOpen = csv.reader(taxoFMD)

    taxoGLAUCOMA = open(
        'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyHaralickGLAUCOMA.csv', 'r')
    taxoGLAUCOMAOpen = csv.reader(taxoGLAUCOMA)

    taxoKTH = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyHaralickKTH.csv', 'r')
    taxoKTHOpen = csv.reader(taxoKTH)

    taxoOUTEX = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/CSV/taxonomyHaralickOUTEX.csv', 'r')
    taxoOUTEXOpen = csv.reader(taxoOUTEX)

    n = ""
    if (classe == "COVID"):
        n = 'images'
        o = taxoCovidOpen
    elif (classe == "GLAUCOMA"):
        n = 'GLAUCOMA'
        o = taxoGLAUCOMAOpen
    elif (classe == "KTH"):
        n = 'KTH'
        o = taxoKTHOpen
    elif (classe == "OUTEX"):
        n = "OUTEX"
        o = taxoOUTEXOpen

    listTotal = []
    listTotal2 = []
    if (n != ""):
        for r in o:
            listD = r[0]
            listN = r[1]
            list = r[2:]
            float_list = []

            for item in list:
                float_list.append(float(item))

            key_list = ['dossier', 'fichier', 'distance', 'url']
            dist = distance.cityblock(float_list, haralick_features)
            valeur_list = [listD, listN, dist,
                           'C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/image/' + n + '/' + listD + '/' + listN]
            dictio = dict(zip(key_list, valeur_list))
            listTotal.append(dictio)

        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0

        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        print(nb)
        print(nombre)
        pourc = float(nb) * 100 / float(nombre)

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, pourc]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

    if (classe == "TOUT"):
        listTotal = taxonomyHaralickMan(img, "1400", "COVID")
        listTotal += taxonomyHaralickMan(img, "900", "OUTEX")
        listTotal += taxonomyHaralickMan(img, "500", "GLAUCOMA")
        listTotal += taxonomyHaralickMan(img, "810", "KTH")

        def get_dist(listTotal):
            return listTotal.get('distance')

        listTotal.sort(key=get_dist)
        nb = 0
        for i in range(0, int(nombre)):
            if (listTotal[i]['dossier'] == listTotal[0]['dossier']):
                nb = nb + 1

        for i in range(0, int(nombre)):
            key_list2 = ['dossier', 'fichier', 'distance', 'url', 'nbCovid', 'pourcentage']
            valeur_list2 = [listTotal[i]['dossier'], listTotal[i]['fichier'], listTotal[i]['distance'],
                            listTotal[i]['url'],
                            nb, listTotal[i]['pourcentage']]
            dictio2 = dict(zip(key_list2, valeur_list2))
            listTotal2.append(dictio2)

        listTotal2[0]['nbCovid'] = nb

    return listTotal2
