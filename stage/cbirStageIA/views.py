from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from . import data
import cv2


def home_view(request):
    return render(request, 'accueil.html')

def combine_view(request):
    return render(request, 'extractionCombine.html')

def simple_view(request):
    return render(request, 'extractionSimple.html', context={'reponse': 'Faux'})


def w_file(file_obj):
    new_name = 'queryimage.jpg'
    f = open('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/image/' + new_name + "", 'wb')
    for line in file_obj.chunks():
        f.write(line)
    f.close()

def F_upload(request):
    if request.method == "GET":
        return render(request, 'extractionSimple.html')
    else:
        f = request.FILES
        print("form the uploaded file is:", f)
        f_obj = f.get("uploadfile")

        n = f_obj.name
        s = f_obj.size
        distance = request.POST.get('distance','')
        methode = request.POST.get('methode', '')
        nombre = request.POST.get('nombre', '')
        classe = request.POST.get('inlineRadioOptions', '')

        print("The file name is:", n)
        print("The file size is:", s)
        print("The file classe is:", classe)

        w_file(f_obj)

        img = cv2.imread('C:/Users/Kemkoum/Desktop/Sixieme session/stage/cbirStageIA/static/image/queryimage.jpg', 0)
        img = cv2.resize(img, (202, 144))

        if (methode == "haralick" and distance == "eucledienne"):
            images = data.haralickEuc(img, nombre, classe)
        elif (methode == "haralick" and distance == "canberra"):
            images = data.haralickCan(img, nombre, classe)
        elif (methode == "haralick" and distance == "chebyshev"):
            images = data.haralickChe(img, nombre, classe)
        elif (methode == "haralick" and distance == "manathan"):
            images = data.haralickMan(img, nombre, classe)
        elif (methode == "GLCM" and distance == "eucledienne"):
            images = data.GLCMEuc(img, nombre, classe)
        elif (methode == "GLCM" and distance == "canberra"):
            images = data.GLCMCan(img, nombre, classe)
        elif (methode == "GLCM" and distance == "manathan"):
            images = data.GLCMMan(img, nombre, classe)
        elif (methode == "GLCM" and distance == "chebyshev"):
            images = data.GLCMChe(img, nombre, classe)
        elif (methode == "biodiversity" and distance == "eucledienne"):
            images = data.biodiversityEuc(img, nombre, classe)
        elif (methode == "biodiversity" and distance == "canberra"):
            images = data.biodiversityCan(img, nombre, classe)
        elif (methode == "biodiversity" and distance == "manathan"):
            images = data.biodiversityMan(img, nombre, classe)
        elif (methode == "biodiversity" and distance == "chebyshev"):
            images = data.biodiversityChe(img, nombre, classe)
        elif (methode == "taxonomy" and distance == "eucledienne"):
            images = data.taxonomyEuc(img, nombre, classe)
        elif (methode == "taxonomy" and distance == "canberra"):
            images = data.taxonomyCan(img, nombre, classe)
        elif (methode == "taxonomy" and distance == "manathan"):
            images = data.taxonomyMan(img, nombre, classe)
        elif (methode == "taxonomy" and distance == "chebyshev"):
            images = data.taxonomyChe(img, nombre, classe)
        elif (methode == "biodiversityHaralick" and distance == "eucledienne"):
            images = data.biodiversityHaralickEuc(img, nombre, classe)
        elif (methode == "biodiversityHaralick" and distance == "canberra"):
            images = data.biodiversityHaralickCan(img, nombre, classe)
        elif (methode == "biodiversityHaralick" and distance == "manathan"):
            images = data.biodiversityHaralickMan(img, nombre, classe)
        elif (methode == "biodiversityHaralick" and distance == "chebyshev"):
            images = data.biodiversityHaralickChe(img, nombre, classe)
        elif (methode == "biodiversityGLCM" and distance == "eucledienne"):
            images = data.biodiversityGLCMEuc(img, nombre, classe)
        elif (methode == "biodiversityGLCM" and distance == "canberra"):
            images = data.biodiversityGLCMCan(img, nombre, classe)
        elif (methode == "biodiversityGLCM" and distance == "manathan"):
            images = data.biodiversityGLCMMan(img, nombre, classe)
        elif (methode == "biodiversityGLCM" and distance == "chebyshev"):
            images = data.biodiversityGLCMChe(img, nombre, classe)
        elif (methode == "taxonomyHaralick" and distance == "eucledienne"):
            images = data.taxonomyHaralickEuc(img, nombre, classe)
        elif (methode == "taxonomyHaralick" and distance == "canberra"):
            images = data.taxonomyHaralickCan(img, nombre, classe)
        elif (methode == "taxonomyHaralick" and distance == "manathan"):
            images = data.taxonomyHaralickMan(img, nombre, classe)
        elif (methode == "taxonomyHaralick" and distance == "chebyshev"):
            images = data.taxonomyHaralickChe(img, nombre, classe)
        elif (methode == "taxonomyGLCM" and distance == "eucledienne"):
            images = data.taxonomyGLCMEuc(img, nombre, classe)
        elif (methode == "taxonomyGLCM" and distance == "canberra"):
            images = data.taxonomyGLCMCan(img, nombre, classe)
        elif (methode == "taxonomyGLCM" and distance == "manathan"):
            images = data.taxonomyGLCMMan(img, nombre, classe)
        elif (methode == "taxonomyGLCM" and distance == "chebyshev"):
            images = data.taxonomyGLCMChe(img, nombre, classe)
        else:
            images = []

        if(images != []):
            pourc = images[0]['pourcentage']
            nbCovid = images[0]['nbCovid']
            nomC = images[0]['dossier']



        # print(images)
        return render(request, 'extractionSimple.html', context={'reponse': 'Vrai', 'Images': images, 'distance':distance, 'methode':methode, 'nombre' : nombre, 'pourc': pourc, 'nbCovid':nbCovid, 'nomC': nomC})



